import React, { useState } from "react";
import folderStructure from "../folder";
import CreateNewFolderIcon from '@mui/icons-material/CreateNewFolder';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import DeleteIcon from '@mui/icons-material/Delete';

function FolderStructure() {


    const [folderName, setFolderName] = useState("");
    const [subFolderName, setSubFolderName] = useState("");
    const [fileName, setFileName] = useState("");
    const [subFile, setSubFile] = useState("");
    const [folders, setFolders] = useState(folderStructure);

    const [folderId, setFolderId] = useState();
    const [subFolderId, setSubFolderId] = useState();
    const [fileId, setFileId] = useState();
    const [subFileId, setSubFileId] = useState();


    const [isFolderClicked, setIsClicked] = useState(false);
    const [isSubFolderClicked, setIsSubFolderClick] = useState(false);

    const [isAddFolderClicked, setAddFolderClicked] = useState(false);
    const [isAddFileClicked, setAddFileClicked] = useState(false);

    const [isFolderOpen, setisFolderOpen] = useState(false);
    const [isSubFolderOpen, setisSubFolderOpen] = useState(false);

    const [isEditing, setIsEditing] = useState(false);
    const [isSubFolderEditing, setIsSubFolderEditing] = useState(false);
    const [isFileEditing, setIsFileEditing] = useState(false);
    const [isSubFileEditing, setIsSubFileEditing] = useState(false);


    console.log(subFileId);
    // ------------------------------------------------handling button-----------------------------------------------
    function handleAdd() {
        if (isAddFolderClicked === false)
            setAddFolderClicked(true);
        else
            setAddFolderClicked(false);

    }

    function handleAddFile() {
        setAddFileClicked(true);

    }
    // --------------------------------------delete------------------------------
    function handleDelete(folderName) {
        const updatedFolder = folders.filter((folder) => {

            return folder.name !== folderName;

        });

        setFolders(updatedFolder);
    }
    function handleSubFolderDelete(folderIndex, subFolderName) {
        setFolders(prevFolders => {
            const updatedFolders = [...prevFolders];
            updatedFolders[folderIndex].subFolder = updatedFolders[folderIndex].subFolder.filter(subFolder => subFolder.name !== subFolderName);
            return updatedFolders;
        });
    }

    function handleFileDelete(folderIndex, files) {
        setFolders(prevValue => {
            const updatedFolders = [...prevValue];
            updatedFolders[folderIndex].files = updatedFolders[folderIndex].files.filter((file) => {
                return file !== files
            });
            return updatedFolders;
        })
    }

    function handleSubFileDelete(folderIndex, subFolderIndex, subFile) {
        setFolders(prevValue => {
            const updatedFolders = [...prevValue];
            updatedFolders[folderIndex].subFolder[subFolderIndex].files = updatedFolders[folderIndex].subFolder[subFolderIndex].files.filter((file) => {
                return file !== subFile;
            });
            return updatedFolders;
        })
    }


    // ------------------------------------------------------  handle click  -----------------------------------------------
    function handleClick(e, i) {
        setFolderId(i)
        if (isFolderClicked === false) {
            setIsClicked(true);
            setIsEditing(false);
            setIsFileEditing(false);
            setAddFolderClicked(false);
        } else {
            setIsClicked(false);
            setIsEditing(false);
            setIsFileEditing(false);
            setAddFolderClicked(false);
        }
        if (isFolderOpen === false)
            setisFolderOpen(true);
        else
            setisFolderOpen(false);



    }

    function handleSubFolderClick(e, subFolderName) {
        if (isSubFolderOpen === false) {
            setisSubFolderOpen(true);
            setIsSubFolderEditing(false);
            setIsEditing(false);
            setIsFileEditing(false);
            setIsSubFileEditing(false);

        } else {
            setisSubFolderOpen(false);
        }


        if (isSubFolderClicked === false) {
            setIsSubFolderClick(true);
            setIsSubFolderEditing(false);
        }
        else {
            setIsSubFolderClick(false);
            setIsSubFolderEditing(false);
        }
        setSubFolderId(subFolderName);


    }
    // ---------------------------------------------------------------------handle change----------------
    function handleSubChange(e) {
        setSubFolderName(e.target.value);
    }

    function handleFolderChange(e) {
        setFolderName(e.target.value);
    }

    function handleFileChange(e) {
        setFileName(e.target.value);
    }
    function handleSubFileChange(e) {
        setSubFile(e.target.value);
    }


    // --------------------------------------------------- handle key down----------------------------------
    function handleKeyDown(e) {
        if (e.key === "Enter") {
            setFolders(prevFolders => [...prevFolders, {
                name: folderName,
                subFolder: [{
                    name: subFolderName,
                    files: []
                }],
                files: []
            }]);

            setAddFolderClicked(false);

            setFolderName("");
        }
    }

    function handleFileKeyDown(e, folderIndex) {
        if (e.key === "Enter") {
            setFolders(prevFolders => {
                const updatedFolders = [...prevFolders];
                updatedFolders[folderIndex].files.push(fileName);
                return updatedFolders;
            });
            setIsClicked(false);
            setAddFileClicked(false);
            setFileName("");
        }
    }

    function handleSubFolderKeyDown(e, folderIndex) {
        if (e.key === "Enter") {
            setFolders(prevFolders => {
                const updatedFolders = [...prevFolders];
                updatedFolders[folderIndex].subFolder.push({
                    name: subFolderName,
                    files: []
                });

                setAddFolderClicked(false);
                setSubFolderName("");
                return updatedFolders;
            })

        }
    }
    function handleSubFileKeyDown(e, folderIndex, subFolderIndex) {
        if (e.key === "Enter") {
            setFolders(prevFolders => {
                const updatedFolders = [...prevFolders];

                updatedFolders[folderIndex].subFolder[subFolderIndex].files.push(subFile);
                setAddFileClicked(false);

                setSubFile("");
                return updatedFolders;
            })
        }
    }

    // ----------------------------------------------------handle edit----------------------------------------------------


    function handleFolderEdit() {
        setIsEditing(true);
    }
    function handleEditChange(e) {
        setFolderName(e.target.value);
    }

    function handleEditFolderKeyDown(e, folderIndex) {

        if (e.key === "Enter") {
            setFolders(preValue => {
                const updatedFolders = [...preValue];
                updatedFolders[folderIndex].name = folderName;
                return updatedFolders;
            })

            setAddFolderClicked(false);
            setIsEditing(false);
            setFolderName("");
        }
    }

    function handleSubFolderEdit() {

        setIsSubFolderEditing(true);

    }

    function handlesubFolderEditChange(e) {
        setSubFolderName(e.target.value);
    }

    function handleEditSubFolderKeyDown(e, folderIndex, subFolderIndex) {

        if (e.key === "Enter") {
            setFolders(preValue => {
                const updatedFolders = [...preValue];
                updatedFolders[folderIndex].subFolder[subFolderIndex].name = subFolderName;
                return updatedFolders;
            });
            setIsEditing(false);
            setSubFolderName("");
        }

    }

    function handleFileEdit(e, file) {
        setIsFileEditing(true);
        setFileId(file);

    }
    function handleFileEditChange(e) {
        setFileName(e.target.value);
    }
    function handleEditFileKeyDown(e, folderIndex, fileIndex) {
        if (e.key === "Enter") {
            setFolders(prevValue => {
                const updatedFolders = [...prevValue];
                updatedFolders[folderIndex].files[fileIndex] = fileName;
                return updatedFolders;
            });
            setIsFileEditing(false);
        }
    }
    // --------------------------subFile Edit------------------------------

    function handleSubFileEdit(e, subFile) {
        setIsSubFileEditing(true);
        setSubFileId(subFile);
    }
    function handleSubFileEditChange(e) {
        setSubFile(e.target.value);
    }
    function handleEditSubFileKeyDown(e, folderIndex, subFolderIndex, subFileIndex) {
        if (e.key === "Enter") {
            setFolders(preValue => {
                const updatedFolders = [...preValue];
                updatedFolders[folderIndex].subFolder[subFolderIndex].files[subFileIndex] = subFile;
                return updatedFolders;
            })
            setIsSubFileEditing(false);
        }
    }

    return (
        
        <div className="container">
            <div className="btnGrp">
                <div>

                    <p onClick={handleAdd}><CreateNewFolderIcon style={{ color: "white" }} /></p>
                </div>
                <div>
                    <p onClick={handleAddFile}><NoteAddIcon style={{ color: "white" }} /></p>
                </div>

            </div>

            <input onKeyDown={handleKeyDown} placeholder="Add folder" onChange={handleFolderChange} style={{ display: isAddFolderClicked && !isFolderClicked ? "block" : "none" }} type="text" value={folderName} name="folderName" />
            <div>
                {folders.map((folder, folderIndex) => (

                    <div key={folderIndex}>
                        {
                            isEditing && folderId === folderIndex ? <input placeholder="edit" type="text" onKeyDown={(e) => handleEditFolderKeyDown(e, folderIndex)} onChange={(e) => handleEditChange(e)} defaultValue={folder.name} />
                                :
                                <div className="folder">
                                    <div>
                                        <h2 onDoubleClick={handleFolderEdit} style={{ backgroundColor: isFolderClicked === true && folderId === folderIndex ? "grey" : "#20252C", width: "fit-content" }} onClick={(e) => handleClick(e, folderIndex)}>{folder.name} </h2>

                                    </div>
                                    <div className="deleteBtn">
                                        <button style={{ backgroundColor: "#20252C", color: "white", border: "none" }} onClick={() => handleDelete(folder.name)}><DeleteIcon /></button>

                                    </div>
                                </div>


                        }

                        <input onKeyDown={(e) => handleSubFolderKeyDown(e, folderIndex)}
                            placeholder="Add subfolder"
                            onChange={handleSubChange}
                            style={{ display: isAddFolderClicked && isFolderClicked && folderId === folderIndex ? "block" : "none" }}
                            type="text"
                            value={subFolderName}
                            name="subFolderName" />

                        <input onKeyDown={(e) => handleFileKeyDown(e, folderIndex)}
                            placeholder="Add file"
                            onChange={handleFileChange}
                            style={{ display: folderId === folderIndex && isAddFileClicked && isFolderClicked && !isSubFolderClicked ? "block" : "none" }}
                            type="text"
                            value={fileName}
                            name="fileName" />
                        {/* 
                       ---------------------------subfolder------------------------------ */}

                        {folder.subFolder.map((subFolder, subFolderIndex) => {

                            return (

                                <div style={{ display: isFolderOpen && folderId === folderIndex ? "block" : "none" }} key={subFolderIndex}>
                                    {
                                        isSubFolderEditing && subFolderId === subFolder.name ? <input style={{ marginLeft: "16px" }} placeholder="edit" type="text" onKeyDown={(e) => handleEditSubFolderKeyDown(e, folderIndex, subFolderIndex)} onChange={(e) => handlesubFolderEditChange(e)} defaultValue={subFolder.name} />
                                            : <div className="folder">
                                                <div>
                                                    <h3 onDoubleClick={handleSubFolderEdit} onClick={(e) => handleSubFolderClick(e, subFolder.name)} style={{ marginLeft: "16px", backgroundColor: isSubFolderClicked && subFolderId === subFolder.name ? "grey" : "#20252C" }}>{subFolder.name}   </h3>

                                                </div>
                                                <div className="deleteBtn">
                                                    <button style={{ backgroundColor: "#20252C", color: "white", border: "none" }} onClick={() => handleSubFolderDelete(folderIndex, subFolder.name)}><DeleteIcon /></button>

                                                </div>
                                            </div>

                                    }


                                    <input placeholder="Add subfile" style={{ display: isSubFolderClicked && subFolderId === subFolder.name && isAddFileClicked ? "block" : "none", marginLeft: "16px" }} type="text" onKeyDown={(e) => handleSubFileKeyDown(e, folderIndex, subFolderIndex)} onChange={handleSubFileChange} value={subFile} />

                                    {subFolder.files.map((subFile, subFileIndex) => {
                                        return (
                                            <div style={{ marginLeft: "32px", display: isSubFolderOpen && subFolderId === subFolder.name ? "block" : "none" }} key={subFileIndex}>

                                                {
                                                    isSubFileEditing && subFileId === subFile ? <input placeholder="edit" type="text" onKeyDown={(e) => handleEditSubFileKeyDown(e, folderIndex, subFolderIndex, subFileIndex)} onChange={(e) => handleSubFileEditChange(e)} defaultValue={subFile} />
                                                        : <div className="folder">
                                                            <div>
                                                                <p onDoubleClick={(e) => handleSubFileEdit(e, subFile)}>{subFile} </p>

                                                            </div>
                                                            <div className="deleteBtn">
                                                                <button style={{ backgroundColor: "#20252C", color: "white", border: "none" }} onClick={() => handleSubFileDelete(folderIndex, subFolderIndex, subFile)}><DeleteIcon /></button>

                                                            </div>
                                                        </div>


                                                }

                                            </div>
                                        )
                                    })}
                                </div>


                            )
                        })}

                        {folder.files.map((file, fileIndex) => {

                            return (
                                <div style={{ marginLeft: "16px", display: isFolderOpen && folderId === folderIndex ? "block" : "none" }} key={fileIndex} >
                                    {
                                        isFileEditing && fileId === file ? <input placeholder="edit" type="text" onKeyDown={(e) => handleEditFileKeyDown(e, folderIndex, fileIndex)} onChange={(e) => handleFileEditChange(e)} defaultValue={file} />
                                            : <div className="folder"> 
                                                <div>
                                                <span onDoubleClick={(e) => handleFileEdit(e, file)}>{file} </span>

                                                </div>
                                                <div className="delteBtn">
                                                <button  style={{ backgroundColor: "#20252C", color: "white", border: "none" }} onClick={() => handleFileDelete(folderIndex, file)}><DeleteIcon/></button>

                                                </div>
                                            </div>


                                    }



                                </div>
                            )
                        }


                        )}
                    </div>
                ))}
            </div>
        </div>
    );
}

export default FolderStructure;
