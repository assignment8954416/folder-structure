const folderStructure = [{
    name: "Documents",
    subFolder:[],
    files: ["Document1.jpg",
        "Document2.jpg",
        "Document3.jpg"]
},
{
    name: "Desktop",
    subFolder: [{
        name: "Drivers",
        files: ["Printerdriver.dmg",
            "cameradriver.dmg"]
    }],
    files:[]
},
{
    name: "Downloads",
    subFolder:[],
    files: ["Printerdriver.dmg",
        "cameradriver.dmg"]
},
{
    name: "Applications",
    subFolder:[],
    files: ["Webstorm.dmg",
        "Pycharm.dmg",
        "FileZila.dmg",
        "Mattermost.dmg",]
},





]

export default folderStructure;